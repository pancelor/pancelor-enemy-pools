local dev,dev_log_replacements,dev_log_skips
-- dev=true
dev_log_replacements=dev
-- dev_log_skips=dev

local enemyPool = {
  _registry={},
}

local components = require "necro.game.data.Components"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local utils = require "system.utils.Utilities"

local RNG_CHANNEL=121416

local function cmp(a,b)
  return a<b and -1
    or a>b and 1
    or 0
end

local function registrySortComp(a,b)
  local res

  res=cmp(-a.priority,-b.priority) -- note: reversed
  if res~=0 then return res==-1 end

  res=cmp(a.fromName,b.fromName)
  if res~=0 then return res==-1 end

  res=cmp(a.toName,b.toName)
  if res~=0 then return res==-1 end

  res=cmp(a.probability,b.probability)
  if res~=0 then return res==-1 end

  return false
end

event.levelLoad.add("resetConversionRegistry", {order="entities", sequence=-1000}, function(ev)
  -- dbg("resetRegistry")
  enemyPool._registry={}
end)

-- call this during levelLoad{order="entities"}
function enemyPool.registerConversion(opts)
  -- dbg("registerConversion")
  local from,to,probability,priority=opts.from,opts.to,opts.probability,opts.priority
  if not probability then
    probability=1
    priority=-100
  end
  if not priority then
    priority=priority or 0 -- higher priority conversions happen first
  end

  table.insert(enemyPool._registry,{
    fromName=from,
    toName=to,
    probability=probability,
    priority=priority,
  })
end

event.levelLoad.add("doConvertEnemies", {order="entities", sequence=1000}, function(ev)
  -- dbg("doConvertEnemies")
  table.sort(enemyPool._registry,registrySortComp)
  for _, entry in ipairs(enemyPool._registry) do
    local old=ecs.getEntitiesByType(entry.fromName)
    while old:next() do
      local proc=rng.roll(entry.probability,RNG_CHANNEL)
      if proc then
        object.spawn(entry.toName,old.position.x,old.position.y)
        object.delete(old)
      end
      if proc and dev_log_replacements then
        dbg("replaced:",entry.fromName,entry.toName)
      end
      if not proc and dev_log_skips then
        dbg("skipped:",entry.fromName,entry.toName)
      end
    end
  end
end)

return enemyPool
