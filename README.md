# pancelor enemy pools

This is a helper mod that lets other mods easily add their custom enemy types to
the game.

## how it works

pancelor-enemy-pools keeps a "registry" of enemy conversions that is
re-calculated during the start of every level:

* Before `event.levelLoad`, the registry is reset.
* During `event.levelLoad`, user mods should register their custom enemy types.
* After `event.levelLoad`, the registry converts the floor's enemies according
to the infomation registered by the user mods.

If multiple user mods want to convert the same base enemy, **all conversions
will happen**.

For example, if mod A adds a registry entry saying "convert 20% of green
slimes to triangle slimes", and mod B adds a registry entry saying "convert 20%
of green slimes to pentagonal slimes", you will end up with 20% triangle slimes,
and 16% pentagonal slimes (20% of the remaining 80%).

User mods can set their conversion priority to happen before/after other
conversions. By default, user mods that convert 100% of one enemy to another
have their conversion priority set low (-100) to give other user mods a chance
to convert that same enemy type.

## how to use

```lua
local enemyPool = require "enemypool.EnemyPool"
local currentLevel = require "necro.game.level.CurrentLevel"

event.levelLoad.add("registerMyCoolSlime", {order="entities"}, function(ev)
  -- convert 30% of green slimes to custom slimes
  enemyPool.registerConversion{
    from="Slime",
    to="mymod_slime",
    probability=0.3,
  }

  -- convert all blue slimes to custom slimes
  enemyPool.registerConversion{
    from="Slime2",
    to="mymod_slime",
  }

  -- change conversion priority
  enemyPool.registerConversion{
    from="Slime3",
    to="mymod_slime",
    priority=3,
  }

  -- conditional conversion example
  if currentLevel.getZone()==3 then
    local probability=currentLevel.getFloor()==2 and 0.8 or 0.1
    enemyPool.registerConversion{
      from="Slime4",
      to="mymod_slime",
      probability=probability,
    }
    enemyPool.registerConversion{
      from="Slime5",
      to="mymod_slime",
      probability=probability,
    }
  end
end)
```

Check out https://gitlab.com/pancelor/triangle-slime
or https://gitlab.com/pancelor/penta-slime for an example of this in use.
